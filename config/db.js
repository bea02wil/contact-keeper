const mongoose = require("mongoose");
const config = require("config");
const db = config.get("mongoURI"); // грабим значение monoURI из default.json

//Второй такой параметр у connect избавляет от варнингов в консоле
const connectDB = async () => {
  try {
    await mongoose.connect(db, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    });

    console.log("MongoDB Connected...");
  } catch (err) {
    console.error(err.message);
    process.exit(1); //выход с failure - отказом
  }
};

module.exports = connectDB;
