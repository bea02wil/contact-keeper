const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const { check, validationResult } = require("express-validator"); //Уже нужно просто подключать  express-validator вместо express-validator/check
const jwt = require("jsonwebtoken");
const config = require("config");

const User = require("../models/User");

// @route          POST api/users
// @descriptions   Register a user
// @access         Public
//res.send(req.body) чтобы в body (наприм. в postman) отправить наш json и вот ответ получим этот json (по этому коду) - (здесь так было)
//Второй параметр в post - [] - это валидация.
//Первый параметр в check - это имя переменной, второй сообщение
//not().isEmpty() - указываем что не эмпти должно быть
//методы через точку после check - это rules - правила
router.post(
  "/",
  [
    check("name", "Please add name").not().isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check(
      "password",
      "Please enter a password with 6 or more characters"
    ).isLength({
      min: 6,
    }),
  ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name, email, password } = req.body;

    try {
      let user = await User.findOne({ email: email });

      if (user) {
        return res.status(400).json({ msg: "User already exists" });
      }

      user = new User({
        name: name,
        email,
        password,
      });

      const salt = await bcrypt.genSalt(10);

      user.password = await bcrypt.hash(password, salt);

      await user.save();

      const payload = {
        user: {
          id: user.id,
        },
      };

      jwt.sign(
        payload,
        config.get("jwtSecret"),
        {
          expiresIn: 360000,
        },
        (err, token) => {
          if (err) throw err;
          res.json({ token });
        }
      );
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);

module.exports = router;
